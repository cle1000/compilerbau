
%option yylineno
%{
#include <stdio.h>
#include <string.h>
//#include <y.tab.h>
#define MAX(a, b) ((a > b)?a :b )

//re#defining YYSTYPE

//extern YYSTYPE yylval;

typedef enum {
	NOTHING,	// 0
	SEMICOLON,	// 1
	COLON,		// 2
	BOOLEAN,	// 3
	IDENTIFIER,	// 4
	NUMBER,		// 5
	OPERATOR,	// 6
	ASS,		// 7
	FOR,		// 8
	WHILE,		// 9
	IF,			// 10
	ELSE,		// 11
	B_OPEN,		// 12
	B_CLOSE,	// 13
	BC_OPEN,	// 14
	BC_CLOSE,	// 15
	A_OPEN,		// 16
	A_CLOSE,	// 17
	DECL_MOD,	// 18
	STRING,		// 19
	TYPE,		// 20
	UN_OP,		// 21
	REL_OP,		// 22
	ADD_OP,		// 23
	MUL_OP,		// 24
	UN_ASS_OP,	// 25
	FUNCALL 	// 26
} token;
#define MAX_FIRST_SET_LEN 10

typedef enum {
	r_program,			
	r_compound_stmt,
	r_decl_statements,
	r_decl_stmt,
	r_variables,
	r_variable,
	r_statements,
	r_statement,
	r_if_stmt,
	r_while_stmt,
	r_for_stmt,
	r_assign_expr,
	r_assign_stmt,
	r_funcall_stmt,
	r_funcall_args,
	r_paren_expr,
	r_expression,
	r_simple_expr,
	r_term,
	r_factor,
	r_un_ass_op,
	r_rel_op,
	r_rel_op_,
	r_add_op,
	r_mul_op,
	r_decl_mod,
	r_type,
	r_number,
	r_int_lit,
	r_string,
	r_identifier,
	r_text,
	r_bool_lit,
	r_un_op,
	r_digit,
	r_alpha
} rules;

token first_set[36][MAX_FIRST_SET_LEN] = {  
   {BC_OPEN,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// program 			= 0
   {BC_OPEN,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// compound_stmt	= 1
   {TYPE,DECL_MOD,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,     	// decl_statements
   {TYPE,DECL_MOD,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   		// decl_stmt
   {IDENTIFIER,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   // variables
   {IDENTIFIER,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   // variable
   {IF,WHILE,FOR,BC_OPEN,FUNCALL,IDENTIFIER,NOTHING,NOTHING,NOTHING,NOTHING} ,   			// statements
   {IF,WHILE,FOR,BC_OPEN,FUNCALL,IDENTIFIER,NOTHING,NOTHING,NOTHING,NOTHING} ,   		    // statement
   {IF,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   		// if_stmt
   {WHILE,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   		// while_stmt
   {FOR,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   		// for_stmt
   {IDENTIFIER,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   // assign_expr
   {IDENTIFIER,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   // assign_stmt
   {FUNCALL,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// funcall_stmt
   {IDENTIFIER,STRING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// funcall_args  
   {B_OPEN,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// paren_expr
   {UN_OP,NUMBER,BOOLEAN,B_OPEN,IDENTIFIER,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// expression
   {UN_OP,NUMBER,BOOLEAN,B_OPEN,IDENTIFIER,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// simple_expr
   {UN_OP,NUMBER,BOOLEAN,B_OPEN,IDENTIFIER,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// term
   {UN_OP,NUMBER,BOOLEAN,B_OPEN,IDENTIFIER,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// factor
   {UN_ASS_OP,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// un_ass_op
   {REL_OP,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// rel_op
   {REL_OP,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// rel_op_
   {ADD_OP,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// add_op
   {MUL_OP,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// mul_op
   {DECL_MOD,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// decl_mod
   {TYPE,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   		// type
   {NUMBER,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// number
   {NUMBER,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// int_lit
   {STRING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// string
   {IDENTIFIER,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   // identifier
   {IDENTIFIER,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   // text
   {BOOLEAN,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// bool_lit
   {UN_OP,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	    // un_op
   {NUMBER,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING} ,   	// digit
   {IDENTIFIER,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING,NOTHING}     // alpha
};

const char * const tokenNames[] = { "NOTHING",
									"SEMICOLON",
									"COLON",
									"BOOLEAN",
									"IDENTIFIER",
									"NUMBER",
									"OPERATOR",
									"ASS",
									"FOR",
									"WHILE",
									"IF",
									"ELSE",
									"B_OPEN",
									"B_CLOSE",
									"BC_OPEN",
									"BC_CLOSE",
									"A_OPEN",
									"A_CLOSE",
									"DECL_MOD",
									"STRING",
									"TYPE",
									"UN_OP",
									"REL_OP",
									"ADD_OP",
									"MUL_OP",
									"UN_ASS_OP",
									"FUNCALL"
								 };

const char * const ruleNames[] = {
	"program", 			
	"compound_stmt"	,
	"decl_statements",
	"decl_stmt",
	"variables",
	"variable",
	"statements",
	"statement",
	"if_stmt",
	"while_stmt",
	"for_stmt",
	"assign_expr",
	"assign_stmt",
	"funcall_stmt",
	"funcall_args",
	"paren_expr",
	"expression",
	"simple_expr",
	"term",
	"factor",
	"un_ass_op",
	"rel_op",
	"rel_op_",
	"add_op",
	"mul_op",
	"decl_mod",
	"type",
	"number",
	"int_lit",
	"string",
	"identifier",
	"text",
	"bool_lit",
	"un_op",
	"digit",
	"alpha"
};
	

token current_token=-1;


int modula_comments_open=0;
int nr_modula_comments=0;
int max_modula_nested=1;

void statement(void);
void compound_stmt(void);
void expression(void);
void parent_expr(void);
void assign_stmt(void);
int is_in_first_set(rules, token);

%}

tab					[\t]
newline				[\r\n]
space				[ ]

bool_value          (true)|(false)
string              \".*\"

data_type           int|boolean|float|char

identifier			([A-Za-z_])([A-Za-z0-9_]*)
number				([0-9])([0-9]*)

semicolon			;
colon               ,

ass_op				=|:=
for					(for)
if					(if)
else				(else)
while				(while)
funcall 			read|write
un_ass_op           {a_plusplus}|{a_minusminus}
rel_op              {c_unequal}|{c_ge}|{c_geq}|{c_le}|{c_leq}|{c_equal}
add_op              {a_plus}|{a_minus}|{or}
mul_op              {a_mul}|{a_div}|{a_percent}|{and}

c_equal				(==)
c_unequal			(!=)
c_leq				(<=)
c_geq				(>=)
c_ge				(>)
c_le				(<)
a_plus				\+
a_plusplus			\+\+
a_minusminus		\-\-
b_open				\(
b_close             \)
a_minus				-
a_div				\/
a_mul				\*
a_percent           \%
arithmetic          {a_plus}|{a_plusplus}|{a_minus}|{a_minusminus}|{a_div}|{a_mul}|{a_percent}
and					\&\&
or					\|\|
a_open              \[
a_close             \]
bc_open				\{
bc_close            \}
decl_mod			(final)
any					.

%x comment

%%
"/*".*"*/"				/* ignore comment */;
"//".*					/* ignore comment */;
"(*"					{
							modula_comments_open++; 
							if(modula_comments_open>0) BEGIN(comment);
							/* ignore comment */;
						}
<comment>"(*"			{
							modula_comments_open++; 
							max_modula_nested = MAX(max_modula_nested, modula_comments_open);
						}
						<comment>[^*(\n]*	/* ignore comment */; /* eat anything that's not a '*' */
<comment>"*"+[^*)(\n]*   /* ignore comment */; /* eat up '*'s not followed by ')'s */
<comment>"*"+")"		{
							if(modula_comments_open>0) nr_modula_comments++;
							modula_comments_open--; 
							if(modula_comments_open==0)	{ BEGIN(INITIAL); /* ignore comment */;}
						}
{decl_mod}				return DECL_MOD; /*yylval= yytext;*/ 
{string}				return STRING;
{bool_value}			return BOOLEAN;
{number}				return NUMBER;
{for}					return FOR;
{while}					return WHILE;
{if}					return IF;
{else}					return ELSE;
{ass_op}				return ASS;
{un_ass_op}				return UN_ASS_OP;
{rel_op}				return REL_OP;
{add_op}				return ADD_OP;
{mul_op}				return MUL_OP;
{a_open}				return A_OPEN;
{a_close}				return A_CLOSE;
{data_type}				return TYPE;
{bc_open}               return BC_OPEN;
{bc_close}              return BC_CLOSE;;
{b_open}				return B_OPEN;
{b_close}				return B_CLOSE;
{semicolon}             return SEMICOLON;
{funcall}				return FUNCALL;
{colon}					return COLON;
{newline}				/* ignore end of line */;
{tab}					/* ignore tabs */;
{space}					/* ignore spaces */;
{identifier}			return IDENTIFIER;
{any}					printf("\nLexing error at line: %d with character: %s\n", yylineno, yytext); exit(0);
%%


void error(char* text){
	printf("\nParsing error [ %s ] at line: %d with token: %s\n", text, yylineno, yytext);
	exit(0);
}

void getToken(){
	int r = yylex();
	if (r == 0) exit(0);
	//printf("{{Line: %d, TokenNr: %d, Token: %d}}\n",yylineno, i, r);
	//if(!is_in_first_set(current_token, r)) printf("Token %i is not allowed after %i\n",r,current_token);
	current_token = r;
}

int is_in_first_set(rules rule, token next){
	token *first = first_set[rule];
	for(int i=0; i<MAX_FIRST_SET_LEN;i++){
		if(first[i]==next) return 1;
	}
	return 0;
}

int accept(token t) {
    if (current_token == t) {
        getToken();
        return 1;
    }
    return 0;
}

int lookahead(token t){
	return (current_token == t) ? 1 : 0;
}

int look_next(){
	return current_token;
}

int expect(token t) {
    if (accept(t))
        return 1;
    
	char e[100];
	sprintf(e, "Expected %s but found %s", tokenNames[t], tokenNames[current_token]);
	error(e);

    return 0;
}
void check_first(rules r){
	if(!is_in_first_set(r, current_token)) {	
		char e[200];
		char firsts_str[100]={0};
		int index = 0;
		token *bla=first_set[r];
		for(int i=0; i<MAX_FIRST_SET_LEN;i++){
			token t = bla[i];
			if(!t==NOTHING) {
				strcat(firsts_str, tokenNames[t]);
				strcat(firsts_str, ", ");
			}
		}
		sprintf(e, "%s : Expected to start with one of %s but found %s", ruleNames[r], firsts_str, tokenNames[current_token]);
		error(e);
	}
}

void factor(void){
	check_first(r_factor);
	accept(UN_OP);
	switch(look_next()){
		case NUMBER: expect(NUMBER); break;
		case BOOLEAN: expect(BOOLEAN); break;
		case B_OPEN: parent_expr(); break;
		case IDENTIFIER: 
			expect(IDENTIFIER);
			if (accept(A_OPEN)){
				expression();
				expect(A_CLOSE);
			}
	}
}

void term(void) {
	check_first(r_term);
	factor();
	if (accept(MUL_OP))
		term();
}

void simple_expr(void){
	check_first(r_simple_expr);
	term();
	if (accept(ADD_OP))
		simple_expr();
}

void expression (void){
	check_first(r_expression);
	simple_expr();
	if (accept(REL_OP))
		simple_expr();
}

void ass_variable (void){
	check_first(r_variable);
	expect(IDENTIFIER);
	if (accept(A_OPEN)){
		expression();
		expect(A_CLOSE);
	}
	if (accept(ASS)){
		if (!(accept(STRING) || accept(BOOLEAN) || accept(NUMBER))) {
			error("no value for assign statement");
		}
	}
}

void variable (void){
	check_first(r_variable);
	expect(IDENTIFIER);
	if (accept(A_OPEN)){
		expression();
		expect(A_CLOSE);
	}
}

void variables(void){
	check_first(r_variables);
	do {
		ass_variable();
	}while (accept(COLON));
}

void parent_expr(void){
	check_first(r_paren_expr);
	expect(B_OPEN);
	expression();
	expect(B_CLOSE);
}

void funcall_args(void){
	check_first(r_funcall_args);
	do{
		if (lookahead(IDENTIFIER)){
			variable();
		}else if (lookahead(STRING)){
			expect(STRING);
		}else{
			error("error in arguments");
		}
	}while(accept(COLON));
}

void funcall_stmt(void){
	check_first(r_funcall_stmt);
	expect(FUNCALL);
	expect(B_OPEN);
	funcall_args();
	expect(B_CLOSE);
	expect(SEMICOLON);
}

void for_stmt(void){
	check_first(r_for_stmt);
	expect(FOR);
	expect(B_OPEN);
	assign_stmt();
	expect(SEMICOLON);
	expression();
	expect(SEMICOLON);
	assign_stmt();
	expect(B_CLOSE);
	statement();
}

void while_stmt(void){
	check_first(r_while_stmt);
	expect(WHILE);
	parent_expr();
	statement();
}

void if_stmt(void){
	check_first(r_if_stmt);
	expect(IF);
	parent_expr();
	statement();
	if (accept(ELSE)){
		statement();
	}
}

void assign_stmt(void){
	check_first(r_assign_stmt);
	variable();
	if (accept(ASS)){
		expression(); 
	}else{
		expect(UN_ASS_OP);
	}
}

int isStatement(){
	switch (look_next()){
		case IF:
		case IDENTIFIER:
		case BC_OPEN:
		case FOR:
		case WHILE:
		case FUNCALL:
		return 1;
	}
	return 0;
}

void statement (void){
	check_first(r_statement);
	switch (look_next()){
		case IF:			if_stmt();			break;
		case IDENTIFIER:	assign_stmt(); 	
							expect(SEMICOLON);	break;
		case BC_OPEN:		compound_stmt();	break;
		case FOR:			for_stmt();			break;
		case WHILE:			while_stmt();		break;
		case FUNCALL:		funcall_stmt();		break;
		default:			error("no statement found");
	}
}

void statements (void){
	check_first(r_statements);
	do{
		statement();
	}while (isStatement());
}

void decl_statement(void){
	check_first(r_decl_stmt);
	accept(DECL_MOD);
	expect (TYPE);
	variables();
	expect(SEMICOLON);
}

void decl_statements(void){
	while(lookahead(DECL_MOD) || lookahead(TYPE)){
		check_first(r_decl_statements);
		decl_statement();
	}
}

void compound_stmt(void){
	check_first(r_compound_stmt);
	expect(BC_OPEN);
	decl_statements();
	statements();
	expect(BC_CLOSE);
}

void program(void) {
    getToken();
    check_first(r_program);
    compound_stmt();
}

int main(int argc, char **argv) {
	program();
	return 0;
}