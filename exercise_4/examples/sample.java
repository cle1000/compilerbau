{ int x, y, z;
  final int n = 10;
  final int a[n];

  // greatest common divisor
  { read(x, y);
    while(x * y != 0)
      if(x > y)
      	x = x - y;
      else y = y - x;
    if(x == 0)
      write("gcd = ", y);
    else write("gcd = ", x);
  }

  // factorial calculation
  { read(x);
    y = 1;
    for(z = 2; z <= x; z++)
      y = y * z;
    write(x, " != ", y);
  }


  // prime number verification
  { int k;
    boolean prime;
    read(x);
    k = 2;
    prime = true;
    while(k < x/2 && prime) {
      if(x % k == 0)
        prime = false;
      k++;
    }
    write(x, " is prime is ",  prime);
  }

  // maximum array element
  { int i, max;
    for(i = 0; i < n; i++)
      read(a[i]);
    max = a[0];
    for(i = 0; i < n; i++)
      if(max > a[i])
        max = a[i];
    write("max = ", max);
  }

  // bubble sort
  { boolean k;
    float r;
    int i;
    k:= true;
    while(k) {
      k = false;
      for(i = 0; i < n-1; i++)
        if(a[i] > a[i+1]) {
          b = a[i];
          a[i] = a[i+1];
          a[i+1] = b;
          k = true;
        }
    }
    write("sorted array: ");
    for(i = 0; i < n; i++)
      write(a[i]);
  }
}
