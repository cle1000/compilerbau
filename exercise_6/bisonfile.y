%{
#include <stdio.h>
#include <stdlib.h>

#include "ast_types.h"
extern int line_num;
int yylex(void);
int yyerror(const char *);
#define YYDEBUG 1

// definitions section
// the root of the abstract syntax tree
node root;

%}

%token SEMICOLON BRACKET TYPE_INT TYPE_FLOAT TYPE_BOOLEAN TYPE_CHAR FINAL IDENT STRING OP_PP OP_MM IF ELSE WHILE FOR READ WRITE TRUE FALSE INT FLOAT OP_LEQ OP_GEQ OP_NEQ OP_EQ OP_OROR OP_ANDAND
%start program

%union
{
  int intVal;
  float floatVal;
  char *stringVal;
  node n;
}

%type<intVal> INT
%type<floatVal> FLOAT
%type<stringVal> STRING
%type<stringVal> IDENT
%type<n> program compStmt varDecList varListType final type varList varList_ ident stmtList statement assignStmt ifStmt whileStmt forStmt ioStmt ioCall paramList paramList_ expr simpleExpr term factor relOp addOp mulOp id_
%error-verbose 

%%
program:        compStmt 													{ $$ = $1; root = $$; } ;
compStmt:       '{' varDecList stmtList '}' 								{ $$ = create_node2(n_COMP_STMT,$2,$3); }
varDecList:     varDecList varListType ';' 									{ $$ = create_node2(n_DECL_LIST,$1,$2);	}
				| 															{ $$ = create_node(n_DECL_LIST); };
varListType:    final type varList 											{ $$ = create_node3(n_DECL,$1,$2,$3); }
final:          FINAL 														{ $$ = create_node(n_FINAL); }
				| 															{ $$ = create_node(n_NONE); };
type:           TYPE_INT 													{ $$ = create_str_node(n_TYPE,"int"); }
				| TYPE_FLOAT												{ $$ = create_str_node(n_TYPE,"float"); }
				| TYPE_BOOLEAN												{ $$ = create_str_node(n_TYPE,"boolean"); }
				| TYPE_CHAR 												{ $$ = create_str_node(n_TYPE,"char"); }
varList:        ident varList_ 												{ $$ = create_node2(n_VAR_LIST,$1,$2); }
				| ident varList_ '=' expr									{ $$ = create_node3(n_VAR_LIST,$1,$2,$4); }
varList_:       ',' varList 												{ $$ = $2; }
				| 															{ $$ = create_node(n_NONE); }
ident:          id_ 														{ $$ = $1; }
				| id_ '[' simpleExpr ']' 									{ $$ = create_node2(n_IDENTIFIER2,$1, $3); } 
id_:			IDENT 														{ $$ = create_str_node(n_IDENTIFIER, yylval.stringVal); }
stmtList:       statement													{ $$ = create_node1(n_STATEMENT_LIST,$1);}
				| stmtList statement										{ $$ = create_node2(n_STATEMENT_LIST,$1, $2); }
statement:      assignStmt ';'												{ $$ = $1; }
                | compStmt 													{ $$ = $1; }
                | ifStmt													{ $$ = $1; }
                | whileStmt													{ $$ = $1; }
                | forStmt													{ $$ = $1; }
                | ioStmt ';' 												{ $$ = $1; }
assignStmt:     ident '=' expr 												{ $$ = create_node1(n_ASSIGN_STMT,create_node2(n_ASSIGN,$1,$3)); }
                | OP_PP ident 												{ $$ = create_node2(n_ASSIGN_STMT,create_op(PLUSPLUS),$2);	}
                | OP_MM ident 												{ $$ = create_node2(n_ASSIGN_STMT,create_op(MINUSMINUS),$2); }
                | ident OP_PP												{ $$ = create_node2(n_ASSIGN_STMT,$1,create_op(PLUSPLUS)); }
                | ident OP_MM												{ $$ = create_node2(n_ASSIGN_STMT,$1,create_op(MINUSMINUS)); }
                

ifStmt:         IF '(' expr ')' statement 									{ $$ = create_node2(n_IF,$3,$5); }
                | IF '(' expr ')' statement ELSE statement 					{ $$ = create_node3(n_IF,$3,$5,$7); }
whileStmt:      WHILE '(' expr ')' statement 								{ $$ = create_node2(n_WHILE,$3,$5); }
forStmt:        FOR '(' assignStmt ';' expr ';' assignStmt ')' statement 	{ $$ = create_node4(n_FOR,$3,$5,$7,$9); }
ioStmt:         ioCall '(' paramList ')'									{ $$ = create_node2(n_IOFUNCCALL,$1,$3); }
ioCall:         READ 														{ $$ = create_int_node(n_IOFUNC,0); }
				| WRITE														{ $$ = create_int_node(n_IOFUNC,1); }

paramList:      expr paramList_ 											{ $$ = create_node2(n_PARAM_LIST,$1,$2); }
				| STRING paramList_ 										{ $$ = create_node2(n_PARAM_LIST, create_str_node(n_STRING_CONST, yylval.stringVal), $2);}
paramList_:     ',' paramList 												{ $$ = $2; }
				| 															{ $$ = create_node(n_NONE); } 
expr:           simpleExpr 													{ $$ = $1; }
				| simpleExpr relOp simpleExpr 								{ $$ = create_node3(n_EXPR,$1,$2,$3); }
simpleExpr:     term 														{ $$ = $1; }
				| term addOp simpleExpr										{ $$ = create_node3(n_EXPR,$1,$2,$3); }
term:           factor 														{ $$ = $1; }
				| factor mulOp term											{ $$ = create_node3(n_EXPR,$1,$2,$3); }
factor:         INT 														{ $$ = create_int_node(n_INT_CONST, yylval.intVal);}
                | FALSE 													{ $$ = create_int_node(n_BOOL_CONST,0);}
                | TRUE 														{ $$ = create_int_node(n_BOOL_CONST,1);}
                | ident 													{ $$ = $1; }
                | '!' factor												{ $$ = create_node2(n_EXPR,create_op(NOT),$2); }
                | '-' factor												{ $$ = create_node2(n_EXPR,create_op(MINUS),$2);}
                | '(' expr ')'  											{ $$ = $2; }
relOp:          '<'															{ $$ = create_op(LT); }
                | OP_LEQ													{ $$ = create_op(LE); }
                | '>' 														{ $$ = create_op(GT); }
                | OP_GEQ													{ $$ = create_op(GE); }
                | OP_NEQ													{ $$ = create_op(NE); }
                | OP_EQ 													{ $$ = create_op(EQ); }

addOp:          '+' 														{ $$ = create_op(PLUS); }
                | '-' 														{ $$ = create_op(MINUS); }
                | OP_OROR 													{ $$ = create_op(OR); }

mulOp:          '*'															{ $$ = create_op(MUL); }
                | '/'														{ $$ = create_op(DIV); }
                | '%'														{ $$ = create_op(MOD); }
                | OP_ANDAND 												{ $$ = create_op(AND); }
%%

int yyerror(const char *s) {
  fprintf(stderr, "%s, (line %d)\n", s, line_num);
  return EXIT_FAILURE;
}

int main(int argc, char **argv) {
    //yydebug = 1; // debug flag: inactive: 0, active: else

    yyparse();
    print_ast(root,1);
    return 0;
}
