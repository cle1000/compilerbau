%{

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "ast_types.h"

#include "bison.h"

int line_num = 1;

void strDup(char* source) {
  yylval.stringVal = malloc(sizeof(char)*strlen(source));
  strcpy(yylval.stringVal, source);
}

%}

whitespace		    [ \r\t]
newLine             [\n]

brackets            [(]|[)]|[{]|[}]|[\[]|[\]]

types               "int"|"float"|"boolean"
functions           "read"|"write"

variable            ([a-z]|[A-Z]|_)([a-z]|[A-Z]|[0-9]|_)*

lineComment         "//"(.*)
inlineComment       \/\*([^*]*)\*\/
comment             {inlineComment}|{lineComment}

digit               [0-9]*
float               {digit}+\.{digit}+
string              \".*\"

%option yylineno
%s nestedComments
%%

{comment}       { /*printf("comment: %s\n", yytext);*/ }

{brackets}      { return *yytext; }

"if"            { return IF; }
"else"          { return ELSE; }
"while"         { return WHILE; }
"for"           { return FOR; }

"true"          { return TRUE; }
"false"         { return FALSE; }

"="             { return *yytext; }
","             { return *yytext; }
";"             { return *yytext; }

"read"          { return READ; }
"write"         { return WRITE; }

"int"           { return TYPE_INT; }
"float"         { return TYPE_FLOAT; }
"boolean"       { return TYPE_BOOLEAN; }
"char"          { return TYPE_CHAR; }
"final"         { return FINAL; }
 
{digit}         { yylval.intVal = atoi(yytext); return INT; }
{float}         { yylval.floatVal = atof(yytext); return FLOAT; }
{string}        { strDup(yytext); return STRING; }
{variable}      { strDup(yytext); return IDENT; }

"++"            { return OP_PP; }
"--"            { return OP_MM; }
"<"             { return *yytext; }
"<="            { return OP_LEQ; }
">"             { return *yytext; }
">="            { return OP_GEQ; }
"!"             { return *yytext; }
"!="            { return OP_NEQ; }
"=="            { return OP_EQ; }
"+"             { return *yytext; }
"-"             { return *yytext; }
"||"            { return OP_OROR; }
"*"             { return *yytext; }
"/"             { return *yytext; }
"&&"            { return OP_ANDAND; }
"%"             { return *yytext; }

<<EOF>>         { printf("\nEnd of source file reached, stop parsing\n"); return -99 /*addToken(_EOF, "end-of-file", yylineno)*/; }
{whitespace}    { ; }
{newLine}       { line_num++; }
.               { printf("\nERROR: lexing does not recognize this as a valid token: '%s' (line %d)\n", yytext, yylineno); /*nbrErrors++;*/ return 0/*UNDEFINED*/; }
%%
