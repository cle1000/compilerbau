/* A Bison parser, made by GNU Bison 3.0.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2013 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 1 "bisonfile.y" /* yacc.c:339  */

#include <stdio.h>
#include <stdlib.h>

#include "ast_types.h"
extern int line_num;
int yylex(void);
int yyerror(const char *);
#define YYDEBUG 1

// definitions section
// the root of the abstract syntax tree
node root;


#line 82 "bison.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif

/* In a future release of Bison, this section will be replaced
   by #include "bison.h".  */
#ifndef YY_YY_BISON_H_INCLUDED
# define YY_YY_BISON_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    SEMICOLON = 258,
    BRACKET = 259,
    TYPE_INT = 260,
    TYPE_FLOAT = 261,
    TYPE_BOOLEAN = 262,
    TYPE_CHAR = 263,
    FINAL = 264,
    IDENT = 265,
    STRING = 266,
    OP_PP = 267,
    OP_MM = 268,
    IF = 269,
    ELSE = 270,
    WHILE = 271,
    FOR = 272,
    READ = 273,
    WRITE = 274,
    TRUE = 275,
    FALSE = 276,
    INT = 277,
    FLOAT = 278,
    OP_LEQ = 279,
    OP_GEQ = 280,
    OP_NEQ = 281,
    OP_EQ = 282,
    OP_OROR = 283,
    OP_ANDAND = 284
  };
#endif

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE YYSTYPE;
union YYSTYPE
{
#line 21 "bisonfile.y" /* yacc.c:355  */

  int intVal;
  float floatVal;
  char *stringVal;
  node n;

#line 159 "bison.c" /* yacc.c:355  */
};
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_BISON_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 174 "bison.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  5
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   134

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  47
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  28
/* YYNRULES -- Number of rules.  */
#define YYNRULES  69
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  114

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   284

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    39,     2,     2,     2,    46,     2,     2,
      37,    38,    44,    43,    34,    40,     2,    45,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,    32,
      41,    33,    42,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    35,     2,    36,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    30,     2,    31,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint8 yyrline[] =
{
       0,    36,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    69,    70,    71,    72,    73,    74,    75,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   101,   102,   103,   105,   106,   107,   108
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 1
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "SEMICOLON", "BRACKET", "TYPE_INT",
  "TYPE_FLOAT", "TYPE_BOOLEAN", "TYPE_CHAR", "FINAL", "IDENT", "STRING",
  "OP_PP", "OP_MM", "IF", "ELSE", "WHILE", "FOR", "READ", "WRITE", "TRUE",
  "FALSE", "INT", "FLOAT", "OP_LEQ", "OP_GEQ", "OP_NEQ", "OP_EQ",
  "OP_OROR", "OP_ANDAND", "'{'", "'}'", "';'", "'='", "','", "'['", "']'",
  "'('", "')'", "'!'", "'-'", "'<'", "'>'", "'+'", "'*'", "'/'", "'%'",
  "$accept", "program", "compStmt", "varDecList", "varListType", "final",
  "type", "varList", "varList_", "ident", "id_", "stmtList", "statement",
  "assignStmt", "ifStmt", "whileStmt", "forStmt", "ioStmt", "ioCall",
  "paramList", "paramList_", "expr", "simpleExpr", "term", "factor",
  "relOp", "addOp", "mulOp", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     123,   125,    59,    61,    44,    91,    93,    40,    41,    33,
      45,    60,    62,    43,    42,    47,    37
};
# endif

#define YYPACT_NINF -40

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-40)))

#define YYTABLE_NINF -1

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int8 yypact[] =
{
     -20,   -40,    22,   -40,    44,   -40,   -40,   -40,    30,    30,
      -1,    27,    29,   -40,   -40,   -40,    17,   119,    12,    24,
      82,   -40,    36,   -40,   -40,   -40,    41,    38,   -40,   -40,
      11,    11,    34,   -40,   -40,   -40,   -40,   -40,    30,   -40,
     -40,    11,    11,   -40,   -40,   -40,   -40,    -2,   -40,   -40,
     -40,    11,    11,    11,   -40,    39,   -12,   -17,    26,    42,
      47,   -40,    48,   -40,    40,    49,    51,    49,    52,   -40,
     -40,   104,   -40,   -40,   -40,   -40,   -40,   -40,    11,   -40,
     -40,   -40,    11,   -40,   -40,   -40,   -40,    11,   104,    11,
      30,    54,   -40,    -2,   -40,   -40,   -40,   -40,    70,   -40,
     -40,   -40,   -40,    59,   -40,    11,   -40,   104,    34,   -40,
     -40,    64,   104,   -40
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,     5,     0,     2,     8,     1,     7,    19,     0,     0,
       0,     0,     0,    38,    39,    23,     0,     0,     0,    17,
       0,    20,     0,    24,    25,    26,     0,     0,    29,    30,
       0,     0,     0,     4,     9,    10,    11,    12,     0,    31,
      32,     0,     0,     3,    21,    22,    27,     0,    52,    51,
      50,     0,     0,     0,    53,     0,    44,    46,    48,     0,
       0,     6,    16,    28,     0,    43,     0,    43,     0,    54,
      55,     0,    58,    60,    61,    62,    57,    59,     0,    65,
      64,    63,     0,    69,    66,    67,    68,     0,     0,     0,
       0,    13,    18,     0,    41,    37,    40,    56,    33,    45,
      47,    49,    35,     0,    15,     0,    42,     0,     0,    14,
      34,     0,     0,    36
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -40,   -40,    97,   -40,   -40,   -40,   -40,    15,   -40,    -4,
     -40,   -40,   -19,   -30,   -40,   -40,   -40,   -40,   -40,    13,
      43,   -24,   -39,    20,   -11,   -40,   -40,   -40
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int8 yydefgoto[] =
{
      -1,     2,    15,     4,    16,    17,    38,    61,    91,    54,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    66,
      94,    67,    56,    57,    58,    78,    82,    87
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      18,    44,    60,    64,    28,    29,    55,    59,     7,    65,
       1,    79,    72,    73,    74,    75,    18,    63,    48,    49,
      50,     7,     5,    80,    39,    40,    81,    68,    18,    76,
      77,    48,    49,    50,    62,    51,    30,    52,    53,    99,
       7,    69,    70,   100,     7,    41,     8,     9,    51,    33,
      52,    53,    98,     6,     7,    83,     8,     9,    10,    42,
      11,    12,    13,    14,    31,   103,    32,    18,    45,   102,
      84,    85,    86,    46,     1,    47,    92,    71,   111,    89,
      88,   109,    90,    93,    18,   107,    62,   105,   110,    95,
      97,   108,     7,   113,     8,     9,    10,     3,    11,    12,
      13,    14,   112,    18,    18,   104,   106,   101,    18,     0,
      96,     0,     1,    43,     7,     0,     8,     9,    10,     0,
      11,    12,    13,    14,    34,    35,    36,    37,     0,     0,
       0,     0,     0,     0,     1
};

static const yytype_int8 yycheck[] =
{
       4,    20,    32,    42,     8,     9,    30,    31,    10,    11,
      30,    28,    24,    25,    26,    27,    20,    41,    20,    21,
      22,    10,     0,    40,    12,    13,    43,    51,    32,    41,
      42,    20,    21,    22,    38,    37,    37,    39,    40,    78,
      10,    52,    53,    82,    10,    33,    12,    13,    37,    32,
      39,    40,    71,     9,    10,    29,    12,    13,    14,    35,
      16,    17,    18,    19,    37,    89,    37,    71,    32,    88,
      44,    45,    46,    32,    30,    37,    36,    38,   108,    32,
      38,   105,    34,    34,    88,    15,    90,    33,   107,    38,
      38,    32,    10,   112,    12,    13,    14,     0,    16,    17,
      18,    19,    38,   107,   108,    90,    93,    87,   112,    -1,
      67,    -1,    30,    31,    10,    -1,    12,    13,    14,    -1,
      16,    17,    18,    19,     5,     6,     7,     8,    -1,    -1,
      -1,    -1,    -1,    -1,    30
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    30,    48,    49,    50,     0,     9,    10,    12,    13,
      14,    16,    17,    18,    19,    49,    51,    52,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    56,    56,
      37,    37,    37,    32,     5,     6,     7,     8,    53,    12,
      13,    33,    35,    31,    59,    32,    32,    37,    20,    21,
      22,    37,    39,    40,    56,    68,    69,    70,    71,    68,
      60,    54,    56,    68,    69,    11,    66,    68,    68,    71,
      71,    38,    24,    25,    26,    27,    41,    42,    72,    28,
      40,    43,    73,    29,    44,    45,    46,    74,    38,    32,
      34,    55,    36,    34,    67,    38,    67,    38,    59,    69,
      69,    70,    59,    68,    54,    33,    66,    15,    32,    68,
      59,    60,    38,    59
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    47,    48,    49,    50,    50,    51,    52,    52,    53,
      53,    53,    53,    54,    54,    55,    55,    56,    56,    57,
      58,    58,    59,    59,    59,    59,    59,    59,    60,    60,
      60,    60,    60,    61,    61,    62,    63,    64,    65,    65,
      66,    66,    67,    67,    68,    68,    69,    69,    70,    70,
      71,    71,    71,    71,    71,    71,    71,    72,    72,    72,
      72,    72,    72,    73,    73,    73,    74,    74,    74,    74
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     4,     3,     0,     3,     1,     0,     1,
       1,     1,     1,     2,     4,     2,     0,     1,     4,     1,
       1,     2,     2,     1,     1,     1,     1,     2,     3,     2,
       2,     2,     2,     5,     7,     5,     9,     4,     1,     1,
       2,     2,     2,     0,     1,     3,     1,     3,     1,     3,
       1,     1,     1,     1,     2,     2,     3,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 36 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = (yyvsp[0].n); root = (yyval.n); }
#line 1336 "bison.c" /* yacc.c:1646  */
    break;

  case 3:
#line 37 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node2(n_COMP_STMT,(yyvsp[-2].n),(yyvsp[-1].n)); }
#line 1342 "bison.c" /* yacc.c:1646  */
    break;

  case 4:
#line 38 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node2(n_DECL_LIST,(yyvsp[-2].n),(yyvsp[-1].n));	}
#line 1348 "bison.c" /* yacc.c:1646  */
    break;

  case 5:
#line 39 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node(n_DECL_LIST); }
#line 1354 "bison.c" /* yacc.c:1646  */
    break;

  case 6:
#line 40 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node3(n_DECL,(yyvsp[-2].n),(yyvsp[-1].n),(yyvsp[0].n)); }
#line 1360 "bison.c" /* yacc.c:1646  */
    break;

  case 7:
#line 41 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node(n_FINAL); }
#line 1366 "bison.c" /* yacc.c:1646  */
    break;

  case 8:
#line 42 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node(n_NONE); }
#line 1372 "bison.c" /* yacc.c:1646  */
    break;

  case 9:
#line 43 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_str_node(n_TYPE,"int"); }
#line 1378 "bison.c" /* yacc.c:1646  */
    break;

  case 10:
#line 44 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_str_node(n_TYPE,"float"); }
#line 1384 "bison.c" /* yacc.c:1646  */
    break;

  case 11:
#line 45 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_str_node(n_TYPE,"boolean"); }
#line 1390 "bison.c" /* yacc.c:1646  */
    break;

  case 12:
#line 46 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_str_node(n_TYPE,"char"); }
#line 1396 "bison.c" /* yacc.c:1646  */
    break;

  case 13:
#line 47 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node2(n_VAR_LIST,(yyvsp[-1].n),(yyvsp[0].n)); }
#line 1402 "bison.c" /* yacc.c:1646  */
    break;

  case 14:
#line 48 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node3(n_VAR_LIST,(yyvsp[-3].n),(yyvsp[-2].n),(yyvsp[0].n)); }
#line 1408 "bison.c" /* yacc.c:1646  */
    break;

  case 15:
#line 49 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = (yyvsp[0].n); }
#line 1414 "bison.c" /* yacc.c:1646  */
    break;

  case 16:
#line 50 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node(n_NONE); }
#line 1420 "bison.c" /* yacc.c:1646  */
    break;

  case 17:
#line 51 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = (yyvsp[0].n); }
#line 1426 "bison.c" /* yacc.c:1646  */
    break;

  case 18:
#line 52 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node2(n_IDENTIFIER2,(yyvsp[-3].n), (yyvsp[-1].n)); }
#line 1432 "bison.c" /* yacc.c:1646  */
    break;

  case 19:
#line 53 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_str_node(n_IDENTIFIER, yylval.stringVal); }
#line 1438 "bison.c" /* yacc.c:1646  */
    break;

  case 20:
#line 54 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node1(n_STATEMENT_LIST,(yyvsp[0].n));}
#line 1444 "bison.c" /* yacc.c:1646  */
    break;

  case 21:
#line 55 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node2(n_STATEMENT_LIST,(yyvsp[-1].n), (yyvsp[0].n)); }
#line 1450 "bison.c" /* yacc.c:1646  */
    break;

  case 22:
#line 56 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = (yyvsp[-1].n); }
#line 1456 "bison.c" /* yacc.c:1646  */
    break;

  case 23:
#line 57 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = (yyvsp[0].n); }
#line 1462 "bison.c" /* yacc.c:1646  */
    break;

  case 24:
#line 58 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = (yyvsp[0].n); }
#line 1468 "bison.c" /* yacc.c:1646  */
    break;

  case 25:
#line 59 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = (yyvsp[0].n); }
#line 1474 "bison.c" /* yacc.c:1646  */
    break;

  case 26:
#line 60 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = (yyvsp[0].n); }
#line 1480 "bison.c" /* yacc.c:1646  */
    break;

  case 27:
#line 61 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = (yyvsp[-1].n); }
#line 1486 "bison.c" /* yacc.c:1646  */
    break;

  case 28:
#line 62 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node1(n_ASSIGN_STMT,create_node2(n_ASSIGN,(yyvsp[-2].n),(yyvsp[0].n))); }
#line 1492 "bison.c" /* yacc.c:1646  */
    break;

  case 29:
#line 63 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node2(n_ASSIGN_STMT,create_op(PLUSPLUS),(yyvsp[0].n));	}
#line 1498 "bison.c" /* yacc.c:1646  */
    break;

  case 30:
#line 64 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node2(n_ASSIGN_STMT,create_op(MINUSMINUS),(yyvsp[0].n)); }
#line 1504 "bison.c" /* yacc.c:1646  */
    break;

  case 31:
#line 65 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node2(n_ASSIGN_STMT,(yyvsp[-1].n),create_op(PLUSPLUS)); }
#line 1510 "bison.c" /* yacc.c:1646  */
    break;

  case 32:
#line 66 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node2(n_ASSIGN_STMT,(yyvsp[-1].n),create_op(MINUSMINUS)); }
#line 1516 "bison.c" /* yacc.c:1646  */
    break;

  case 33:
#line 69 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node2(n_IF,(yyvsp[-2].n),(yyvsp[0].n)); }
#line 1522 "bison.c" /* yacc.c:1646  */
    break;

  case 34:
#line 70 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node3(n_IF,(yyvsp[-4].n),(yyvsp[-2].n),(yyvsp[0].n)); }
#line 1528 "bison.c" /* yacc.c:1646  */
    break;

  case 35:
#line 71 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node2(n_WHILE,(yyvsp[-2].n),(yyvsp[0].n)); }
#line 1534 "bison.c" /* yacc.c:1646  */
    break;

  case 36:
#line 72 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node4(n_FOR,(yyvsp[-6].n),(yyvsp[-4].n),(yyvsp[-2].n),(yyvsp[0].n)); }
#line 1540 "bison.c" /* yacc.c:1646  */
    break;

  case 37:
#line 73 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node2(n_IOFUNCCALL,(yyvsp[-3].n),(yyvsp[-1].n)); }
#line 1546 "bison.c" /* yacc.c:1646  */
    break;

  case 38:
#line 74 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_int_node(n_IOFUNC,0); }
#line 1552 "bison.c" /* yacc.c:1646  */
    break;

  case 39:
#line 75 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_int_node(n_IOFUNC,1); }
#line 1558 "bison.c" /* yacc.c:1646  */
    break;

  case 40:
#line 77 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node2(n_PARAM_LIST,(yyvsp[-1].n),(yyvsp[0].n)); }
#line 1564 "bison.c" /* yacc.c:1646  */
    break;

  case 41:
#line 78 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node2(n_PARAM_LIST, create_str_node(n_STRING_CONST, yylval.stringVal), (yyvsp[0].n));}
#line 1570 "bison.c" /* yacc.c:1646  */
    break;

  case 42:
#line 79 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = (yyvsp[0].n); }
#line 1576 "bison.c" /* yacc.c:1646  */
    break;

  case 43:
#line 80 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node(n_NONE); }
#line 1582 "bison.c" /* yacc.c:1646  */
    break;

  case 44:
#line 81 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = (yyvsp[0].n); }
#line 1588 "bison.c" /* yacc.c:1646  */
    break;

  case 45:
#line 82 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node3(n_EXPR,(yyvsp[-2].n),(yyvsp[-1].n),(yyvsp[0].n)); }
#line 1594 "bison.c" /* yacc.c:1646  */
    break;

  case 46:
#line 83 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = (yyvsp[0].n); }
#line 1600 "bison.c" /* yacc.c:1646  */
    break;

  case 47:
#line 84 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node3(n_EXPR,(yyvsp[-2].n),(yyvsp[-1].n),(yyvsp[0].n)); }
#line 1606 "bison.c" /* yacc.c:1646  */
    break;

  case 48:
#line 85 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = (yyvsp[0].n); }
#line 1612 "bison.c" /* yacc.c:1646  */
    break;

  case 49:
#line 86 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node3(n_EXPR,(yyvsp[-2].n),(yyvsp[-1].n),(yyvsp[0].n)); }
#line 1618 "bison.c" /* yacc.c:1646  */
    break;

  case 50:
#line 87 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_int_node(n_INT_CONST, yylval.intVal);}
#line 1624 "bison.c" /* yacc.c:1646  */
    break;

  case 51:
#line 88 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_int_node(n_BOOL_CONST,0);}
#line 1630 "bison.c" /* yacc.c:1646  */
    break;

  case 52:
#line 89 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_int_node(n_BOOL_CONST,1);}
#line 1636 "bison.c" /* yacc.c:1646  */
    break;

  case 53:
#line 90 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = (yyvsp[0].n); }
#line 1642 "bison.c" /* yacc.c:1646  */
    break;

  case 54:
#line 91 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node2(n_EXPR,create_op(NOT),(yyvsp[0].n)); }
#line 1648 "bison.c" /* yacc.c:1646  */
    break;

  case 55:
#line 92 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_node2(n_EXPR,create_op(MINUS),(yyvsp[0].n));}
#line 1654 "bison.c" /* yacc.c:1646  */
    break;

  case 56:
#line 93 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = (yyvsp[-1].n); }
#line 1660 "bison.c" /* yacc.c:1646  */
    break;

  case 57:
#line 94 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_op(LT); }
#line 1666 "bison.c" /* yacc.c:1646  */
    break;

  case 58:
#line 95 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_op(LE); }
#line 1672 "bison.c" /* yacc.c:1646  */
    break;

  case 59:
#line 96 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_op(GT); }
#line 1678 "bison.c" /* yacc.c:1646  */
    break;

  case 60:
#line 97 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_op(GE); }
#line 1684 "bison.c" /* yacc.c:1646  */
    break;

  case 61:
#line 98 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_op(NE); }
#line 1690 "bison.c" /* yacc.c:1646  */
    break;

  case 62:
#line 99 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_op(EQ); }
#line 1696 "bison.c" /* yacc.c:1646  */
    break;

  case 63:
#line 101 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_op(PLUS); }
#line 1702 "bison.c" /* yacc.c:1646  */
    break;

  case 64:
#line 102 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_op(MINUS); }
#line 1708 "bison.c" /* yacc.c:1646  */
    break;

  case 65:
#line 103 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_op(OR); }
#line 1714 "bison.c" /* yacc.c:1646  */
    break;

  case 66:
#line 105 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_op(MUL); }
#line 1720 "bison.c" /* yacc.c:1646  */
    break;

  case 67:
#line 106 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_op(DIV); }
#line 1726 "bison.c" /* yacc.c:1646  */
    break;

  case 68:
#line 107 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_op(MOD); }
#line 1732 "bison.c" /* yacc.c:1646  */
    break;

  case 69:
#line 108 "bisonfile.y" /* yacc.c:1646  */
    { (yyval.n) = create_op(AND); }
#line 1738 "bison.c" /* yacc.c:1646  */
    break;


#line 1742 "bison.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 109 "bisonfile.y" /* yacc.c:1906  */


int yyerror(const char *s) {
  fprintf(stderr, "%s, (line %d)\n", s, line_num);
  return EXIT_FAILURE;
}

int main(int argc, char **argv) {
    //yydebug = 1; // debug flag: inactive: 0, active: else

    yyparse();
    print_ast(root,1);
    return 0;
}
