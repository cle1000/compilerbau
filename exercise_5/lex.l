%option yylineno
%{
#include <stdio.h>
#include <string.h>
#include "bisonfile.tab.h"
#define MAX(a, b) ((a > b)?a :b )
%}

tab					[\t]
newline				[\r\n]
space				[ ]

bool_value          (true)|(false)
string              \".*\"

data_type           int|boolean|float|char

identifier			([A-Za-z_])([A-Za-z0-9_]*)
number				([0-9])([0-9]*)

semicolon			;
colon               ,

ass_op				=|:=
for					(for)
if					(if)
else				(else)
while				(while)
funcall 			read|write
un_ass_op           {a_plusplus}|{a_minusminus}
rel_op              {c_unequal}|{c_ge}|{c_geq}|{c_le}|{c_leq}|{c_equal}
add_op              {a_plus}|{a_minus}|{or}
mul_op              {a_mul}|{a_div}|{a_percent}|{and}

c_equal				(==)
c_unequal			(!=)
c_leq				(<=)
c_geq				(>=)
c_ge				(>)
c_le				(<)
a_plus				\+
a_plusplus			\+\+
a_minusminus		\-\-
b_open				\(
b_close             \)
a_minus				-
a_div				\/
a_mul				\*
a_percent           \%
arithmetic          {a_plus}|{a_plusplus}|{a_minus}|{a_minusminus}|{a_div}|{a_mul}|{a_percent}
and					\&\&
or					\|\|
a_open              \[
a_close             \]
bc_open				\{
bc_close            \}
decl_mod			(final)
any					.

%x comment

%%
"/*".*"*/"				/* ignore comment */;
"//".*					/* ignore comment */;
{decl_mod}				{return DECL_MOD;}
{string}				{yylval = atoi(yytext); return STRING;}
{bool_value}			{yylval = atoi(yytext); return BOOLEAN;}
{number}				{yylval = atoi(yytext); return NUMBER;}
{for}					{return FOR;}
{while}					{return WHILE;}
{if}					{return IF;}
{else}					{return ELSE;}
{ass_op}				{return ASS;}
{un_ass_op}				{return UN_ASS_OP;}
{rel_op}				{return REL_OP;}
{add_op}				{return ADD_OP;}
{mul_op}				{return MUL_OP;}
{a_open}				{return A_OPEN;}
{a_close}				{return A_CLOSE;}
{data_type}				{return TYPE;}
{bc_open}               {return BC_OPEN;}
{bc_close}              {return BC_CLOSE;}
{b_open}				{return B_OPEN;}
{b_close}				{return B_CLOSE;}
{semicolon}             {return SEMICOLON;}
{funcall}				{yylval = atoi(yytext); return FUNCALL;}
{colon}					{return COLON;}
{newline}				/* ignore end of line */;
{tab}					/* ignore tabs */;
{space}					/* ignore spaces */;
{identifier}			{yylval = atoi(yytext); return IDENTIFIER;}
{any}					{printf("\nLexing error at line: %d with character: %s\n", yylineno, yytext); yyterminate();}
%%