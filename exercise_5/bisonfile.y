%{
#include <stdio.h>
int yylex(void);
int yyerror(char *);
%}
%token SEMICOLON COLON BOOLEAN IDENTIFIER NUMBER OPERATOR ASS FOR WHILE IF ELSE B_OPEN B_CLOSE BC_OPEN BC_CLOSE A_OPEN
	A_CLOSE DECL_MOD STRING TYPE UN_OP REL_OP ADD_OP MUL_OP UN_ASS_OP FUNCALL
%start program
%%

program				:
					| compound_stmt;

compound_stmt		: BC_OPEN decl_statements statements BC_CLOSE;

decl_statements		:
					| decl_stmt SEMICOLON decl_statements;

decl_stmt			: TYPE ass_variables
					| DECL_MOD TYPE ass_variables;

ass_variables		: ass_variable
					| ass_variable COLON ass_variables;

ass_variable		: variable
					| variable ASS expression;

variables			: variable
					| variable COLON variables;

variable			: IDENTIFIER
					| IDENTIFIER A_OPEN expression A_CLOSE;

value				: STRING
					| BOOLEAN
					| NUMBER;

statements			:
					| statement statements;

statement			: assign_stmt
					| compound_stmt
					| if_stmt
					| for_stmt
					| while_stmt
					| funcall_stmt;

if_stmt				: IF paren_expr statement else_part;

else_part           :
					| ELSE statement;

while_stmt          : WHILE paren_expr statement;

for_stmt			: FOR B_OPEN assign_expr SEMICOLON expression SEMICOLON assign_expr B_CLOSE statement;

assign_expr			: variable ASS expression
					| variable UN_ASS_OP;

assign_stmt			: assign_expr SEMICOLON;

funcall_stmt		: FUNCALL B_OPEN funcall_args B_CLOSE SEMICOLON
					| FUNCALL B_OPEN 			  B_CLOSE SEMICOLON;

funcall_args        : arg
					| arg COLON funcall_args;

arg					: variable
					| value;

paren_expr          : B_OPEN expression B_CLOSE;

expression			: simple_expr
					| simple_expr REL_OP expression;

simple_expr			: term
					| term ADD_OP simple_expr;

term                : factor_un
					| factor_un MUL_OP term;

factor_un           : un_op factor;

factor              : NUMBER
					| BOOLEAN
					| paren_expr
					| IDENTIFIER A_OPEN expression A_CLOSE
					| IDENTIFIER;

un_op               :
					| UN_OP;


%%
int main() {
  return yyparse();
}

int yyerror(char *s) {
	fprintf(stderr, "%s with token:\n", s);
	return 0;
}