
%option yylineno
%{
#include <stdio.h>
#include <string.h>
#define MAX(a, b) ((a > b)?a :b )

char errors [99999];
int modula_comments_open=0;
int nr_modula_comments=0;
int max_modula_nested=1;

char* upper_string(char s[]) {
	int c = 0;
	while (s[c] != '\0') {
		if (s[c] >= 'a' && s[c] <= 'z') {
			s[c] = s[c] - 32;
		}
		c++;
	}
	return s;
}

char* add_error_message(int linenumber, char * line){
	char buf [30];
	sprintf(buf, "Error at line: %d - %s\n", linenumber, line);
	strcat(errors,buf);
}

char* lower_string(char s[]) {
	int c = 0;
	while (s[c] != '\0') {
		if (s[c] >= 'A' && s[c] <= 'Z') {
			s[c] = s[c] + 32;
		}
		c++;
	}
	return s;
}

%}

tabs				[\t]+
newlines			[\n]+
spaces				[ ]+

bool_value          (true)|(false)
string              \".*\"

DATA_TYPE           int|boolean|float

IDENTIFIER			([A-Za-z_])([A-Za-z0-9_]*)
NUMBER				([0-9])([0-9]*)
OPERATOR			\+ | ==

semicolon			;
comma               ,

allocation          =|:=
for					{for}\(
ifcall				(if)\(
elsecall			(else)
methodcall          {IDENTIFIER}\(
closecall           (\))
c_equal				(==)
c_unequal			(!=)
c_leq				(<=)
c_geq				(>=)
c_ge				(>)
c_le				(<)
comparison			{c_equal}|{c_unequal}|{c_leq}|{c_geq}|{c_ge}|{c_le}
a_plus				\+
a_plusplus			\+\+
a_minusminus		\-\-
a_minus				-
a_div				\/
a_mul				\*
a_percent           \%
arithmetic          {a_plus}|{a_plusplus}|{a_minus}|{a_minusminus}|{a_div}|{a_mul}|{a_percent}
and					\&\&
or					\|\|
boolean_operator    {and}|{or}
oarray              \[
carray              \]
f_final				(final)
flags               {f_final}
any					.*


%x comment


%%
"/*".*"*/"				printf("/** %s ", (yytext+2));
"//".*					printf("/** %s */", (yytext+2));
"(*"					{
							modula_comments_open++; 
							if(modula_comments_open>0) BEGIN(comment);
							printf("/**");
						}
<comment>"(*"			{
							modula_comments_open++; 
							max_modula_nested = MAX(max_modula_nested, modula_comments_open);
						}	
<comment>[^*(\n]*        ECHO; /* eat anything that's not a '*' */
<comment>"*"+[^*)(\n]*   ECHO; /* eat up '*'s not followed by ')'s */
<comment>"*"+")"	    {
							if(modula_comments_open>0) nr_modula_comments++;
							modula_comments_open--; 
							if(modula_comments_open==0)	{ BEGIN(INITIAL); printf("*/");}
						}
{flags}					printf("%s", upper_string(yytext));
{string}				ECHO;
{bool_value}			ECHO;
{NUMBER}				ECHO;
{methodcall}            printf("%s", upper_string(yytext));
{elsecall}				printf("%s", upper_string(yytext));
{closecall}				ECHO;
{allocation}			ECHO;
{comparison}			ECHO;
{arithmetic}			ECHO;
{boolean_operator}		ECHO;
{oarray}				ECHO;
{DATA_TYPE}				printf("%s", upper_string(yytext));
{carray}				ECHO;
\{                      ECHO;
\}                      ECHO;
{semicolon}             ECHO;
{comma}					ECHO;
{newlines}              printf("\n");
^{tabs}					ECHO;
{tabs}					printf("\t");
^{spaces}				ECHO;
{spaces}				printf(" ");
{IDENTIFIER}			printf("%s", lower_string(yytext));
.						add_error_message(yylineno, yytext);
%%

int main(int argc, char **argv) {
	int r = yylex();
	if(modula_comments_open>0) add_error_message(-1, "not all modula comments closed");
	printf("\n%s", errors);
	printf("\n%i modula comments", nr_modula_comments);
	printf("\n%i max modula comments depth\n", max_modula_nested);
	return r;
}