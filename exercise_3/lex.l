
%option yylineno
%{
#include <stdio.h>
#include <string.h>
//#include <y.tab.h>
#define MAX(a, b) ((a > b)?a :b )

//re#defining YYSTYPE

//extern YYSTYPE yylval;

typedef enum {
	NOTHING,	// 0
	SEMICOLON,
	COLON,
	BOOLEAN,
	IDENTIFIER,
	NUMBER,		// 5
	OPERATOR,
	ASS,
	FOR,
	WHILE,
	IF,			// 10
	ELSE,
	B_OPEN,
	B_CLOSE,
	BC_OPEN,
	BC_CLOSE,	// 15
	A_OPEN,
	A_CLOSE,
	DECL_MOD,
	STRING,
	TYPE,		// 20
	UN_OP,
	REL_OP,
	ADD_OP,
	MUL_OP,
	UN_ASS_OP,	// 25
	FUNCALL
} token;

token current_token;


int modula_comments_open=0;
int nr_modula_comments=0;
int max_modula_nested=1;

void statement(void);
void compound_stmt(void);
void expression(void);
void parent_expr(void);
void assign_stmt(void);


%}

tab					[\t]
newline				[\r\n]
space				[ ]

bool_value          (true)|(false)
string              \".*\"

data_type           int|boolean|float|char

identifier			([A-Za-z_])([A-Za-z0-9_]*)
number				([0-9])([0-9]*)

semicolon			;
colon               ,

ass_op				=|:=
for					(for)
if					(if)
else				(else)
while				(while)
funcall 			read|write
un_ass_op           {a_plusplus}|{a_minusminus}
rel_op              {c_unequal}|{c_ge}|{c_geq}|{c_le}|{c_leq}|{c_equal}
add_op              {a_plus}|{a_minus}|{or}
mul_op              {a_mul}|{a_div}|{a_percent}|{and}

c_equal				(==)
c_unequal			(!=)
c_leq				(<=)
c_geq				(>=)
c_ge				(>)
c_le				(<)
a_plus				\+
a_plusplus			\+\+
a_minusminus		\-\-
b_open				\(
b_close             \)
a_minus				-
a_div				\/
a_mul				\*
a_percent           \%
arithmetic          {a_plus}|{a_plusplus}|{a_minus}|{a_minusminus}|{a_div}|{a_mul}|{a_percent}
and					\&\&
or					\|\|
a_open              \[
a_close             \]
bc_open				\{
bc_close            \}
decl_mod			(final)
any					.

%x comment

%%
"/*".*"*/"				/* ignore comment */;
"//".*					/* ignore comment */;
"(*"					{
							modula_comments_open++; 
							if(modula_comments_open>0) BEGIN(comment);
							/* ignore comment */;
						}
<comment>"(*"			{
							modula_comments_open++; 
							max_modula_nested = MAX(max_modula_nested, modula_comments_open);
						}
						<comment>[^*(\n]*	/* ignore comment */; /* eat anything that's not a '*' */
<comment>"*"+[^*)(\n]*   /* ignore comment */; /* eat up '*'s not followed by ')'s */
<comment>"*"+")"		{
							if(modula_comments_open>0) nr_modula_comments++;
							modula_comments_open--; 
							if(modula_comments_open==0)	{ BEGIN(INITIAL); /* ignore comment */;}
						}
{decl_mod}				return DECL_MOD; /*yylval= yytext;*/ 
{string}				return STRING;
{bool_value}			return BOOLEAN;
{number}				return NUMBER;
{for}					return FOR;
{while}					return WHILE;
{if}					return IF;
{else}					return ELSE;
{ass_op}				return ASS;
{un_ass_op}				return UN_ASS_OP;
{rel_op}				return REL_OP;
{add_op}				return ADD_OP;
{mul_op}				return MUL_OP;
{a_open}				return A_OPEN;
{a_close}				return A_CLOSE;
{data_type}				return TYPE;
{bc_open}               return BC_OPEN;
{bc_close}              return BC_CLOSE;;
{b_open}				return B_OPEN;
{b_close}				return B_CLOSE;
{semicolon}             return SEMICOLON;
{funcall}				return FUNCALL;
{colon}					return COLON;
{newline}				/* ignore end of line */;
{tab}					/* ignore tabs */;
{space}					/* ignore spaces */;
{identifier}			return IDENTIFIER;
{any}					printf("\nLexing error at line: %d with character: %s\n", yylineno, yytext); exit(0);
%%


void error(char* text){
	printf("\nParsing error [ %s ] at line: %d with token: %s\n", text, yylineno, yytext);
	exit(0);
}

void getToken(){
	int r = yylex();
	if (r == 0) exit(0);
	//printf("{{Line: %d, TokenNr: %d, Token: %d}}\n",yylineno, i, r);
	current_token = r;
}

int accept(token t) {
    if (current_token == t) {
        getToken();
        return 1;
    }
    return 0;
}

int lookahead(token t){
	return (current_token == t) ? 1 : 0;
}

int look_next(){
	return current_token;
}

int expect(token t) {
    if (accept(t))
        return 1;
    error("unexpected symbol");
    return 0;
}

void factor(void){
	accept(UN_OP);
	switch(look_next()){
		case NUMBER: expect(NUMBER); break;
		case BOOLEAN: expect(BOOLEAN); break;
		case B_OPEN: parent_expr(); break;
		case IDENTIFIER: 
			expect(IDENTIFIER);
			if (accept(A_OPEN)){
				expression();
				expect(A_CLOSE);
			}
	}
}

void term(void) {
	factor();
	if (accept(MUL_OP))
		term();
}

void simple_expr(void){
	term();
	if (accept(ADD_OP))
		simple_expr();
}

void expression (void){
	simple_expr();
	if (accept(REL_OP))
		simple_expr();
}

void ass_variable (void){
	expect(IDENTIFIER);
	if (accept(A_OPEN)){
		expression();
		expect(A_CLOSE);
	}
	if (accept(ASS)){
		if (!(accept(STRING) || accept(BOOLEAN) || accept(NUMBER))) {
			error("no value for assign statement");
		}
	}
}

void variable (void){
	expect(IDENTIFIER);
	if (accept(A_OPEN)){
		expression();
		expect(A_CLOSE);
	}
}

void variables(void){
	do {
		ass_variable();
	}while (accept(COLON));
}

void parent_expr(void){
	expect(B_OPEN);
	expression();
	expect(B_CLOSE);
}

void funcall_args(void){
	do{
		if (lookahead(IDENTIFIER)){
			variable();
		}else if (lookahead(STRING)){
			expect(STRING);
		}else{
			error("error in arguments");
		}
	}while(accept(COLON));
}

void funcall_stmt(void){
	expect(FUNCALL);
	expect(B_OPEN);
	funcall_args();
	expect(B_CLOSE);
	expect(SEMICOLON);
}

void for_stmt(void){
	expect(FOR);
	expect(B_OPEN);
	assign_stmt();
	expect(SEMICOLON);
	expression();
	expect(SEMICOLON);
	assign_stmt();
	expect(B_CLOSE);
	statement();
}

void while_stmt(void){
	expect(WHILE);
	parent_expr();
	statement();
}

void if_stmt(void){
	expect(IF);
	parent_expr();
	statement();
	if (accept(ELSE)){
		statement();
	}
}

void assign_stmt(void){
	variable();
	if (accept(ASS)){
		expression(); 
	}else{
		expect(UN_ASS_OP);
	}
}

int isStatement(){
	switch (look_next()){
		case IF:
		case IDENTIFIER:
		case BC_OPEN:
		case FOR:
		case WHILE:
		case FUNCALL:
		return 1;
	}
	return 0;
}

void statement (void){
	switch (look_next()){
		case IF:			if_stmt();			break;
		case IDENTIFIER:	assign_stmt(); 	
							expect(SEMICOLON);	break;
		case BC_OPEN:		compound_stmt();	break;
		case FOR:			for_stmt();			break;
		case WHILE:			while_stmt();		break;
		case FUNCALL:		funcall_stmt();		break;
		default:			error("no statement found");
	}
}

void statements (void){
	do{
		statement();
	}while (isStatement());
}

void decl_statement(void){
		accept(DECL_MOD);
		expect (TYPE);
		variables();
		expect(SEMICOLON);
}

void decl_statements(void){
	while(lookahead(DECL_MOD) || lookahead(TYPE)){
		decl_statement();
	}
}

void compound_stmt(void){
	expect(BC_OPEN);
	decl_statements();
	statements();
	expect(BC_CLOSE);
}

void program(void) {
    getToken();
    compound_stmt();
}



int main(int argc, char **argv) {
	program();
	return 0;
}