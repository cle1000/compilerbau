%{
#include <stdio.h>
#include <stdlib.h>
extern int line_num;

%}

%token SEMICOLON BRACKET TYPE_INT TYPE_FLOAT TYPE_BOOLEAN TYPE_CHAR FINAL IDENT STRING OP_PP OP_MM IF ELSE WHILE FOR READ WRITE TRUE FALSE INT FLOAT OP_LEQ OP_GEQ OP_NEQ OP_EQ OP_OROR OP_ANDAND
%start program

%union
{
  int intVal;
  float floatVal;
  char *stringVal;
}

%type<intVal> INT
%type<floatVal> FLOAT
%type<stringVal> STRING
%error-verbose 

%%
program:        compStmt ;
compStmt:       '{' varDecList stmtList '}' ;
varDecList:     varDecList varListType ';' | ;
varListType:    final type varList ;
final:          FINAL | ;
type:           TYPE_INT | TYPE_FLOAT | TYPE_BOOLEAN | TYPE_CHAR ;
varList:        ident varList_ | ident varList_ '=' expr ;
varList_:       ',' varList | ;
ident:          IDENT | IDENT '[' simpleExpr ']' ;
stmtList:       statement | statement stmtList ;
statement:      assignStmt ';'
                | compStmt
                | ifStmt
                | whileStmt
                | forStmt
                | ioStmt ';' ;
assignStmt:     ident '=' expr
                | OP_PP ident
                | OP_MM ident
                | ident OP_PP
                | ident OP_MM
                ;

ifStmt:         IF '(' expr ')' statement
                | IF '(' expr ')' statement ELSE statement ;

whileStmt:      WHILE '(' expr ')' statement ;
forStmt:        FOR '(' assignStmt ';' expr ';' assignStmt ')' statement ;
ioStmt:         ioCall '(' paramList ')' ;
ioCall:         READ | WRITE ;

paramList:      expr paramList_ | STRING paramList_ ;
paramList_:     ',' paramList | ;
expr:           simpleExpr | simpleExpr relOp simpleExpr;
simpleExpr:     term | term addOp simpleExpr;
term:           factor | factor mulOp term;
factor:         INT
                | FALSE
                | TRUE
                | ident
                | '!' factor
                | '-' factor
                | '(' expr ')' ;
relOp:          '<'
                | OP_LEQ
                | '>'
                | OP_GEQ
                | OP_NEQ
                | OP_EQ ;

addOp:          '+'
                | '-' 
                | OP_OROR ;

mulOp:          '*'
                | '/'
                | '%'
                | OP_ANDAND ;
%%

int yyerror(char *s) {
  fprintf(stderr, "%s, (line %d)\n", s, line_num);
  return EXIT_FAILURE;
}

int main(int argc, char **argv) {
    // yydebug = 1; // debug flag: inactive: 0, active: else

    yyparse();
    return 0;
}
