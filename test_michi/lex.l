%{
#include <stdio.h>
#include "bison.h"
%}

%%
[0-9]+	{ yylval = atoi(yytext); return NR; }
"+"		{ return ADD_OP; }
"*"		{ return MUL_OP; }
%%
