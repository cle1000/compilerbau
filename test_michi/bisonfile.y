%{
#include <stdio.h>
#include <stdlib.h>

int yylex(void);
int yyerror(const char *);
int res = 0;
%}

%token NR ADD_OP MUL_OP
%start term

%%
term:      fact {res = $1;} term1

term1:                              { $$ = res;}
           | op fact                { if ($1 == 0) {
                                        res = res + $2;
                                      } else { 
                                        res = res * $2;}
                                    } term1 {}

op:        ADD_OP                   {$$ = 0;}
          | MUL_OP                  {$$ = 1;}

fact:      NR                       { $$ = yylval;}

%%

int yyerror(const char *s) {
 fprintf(stderr, "Error\n");
 return EXIT_FAILURE;
}

int main(int argc, char **argv) {
   int r = yyparse();
   printf("RESULT: %d\n", res);
   return r;
}
