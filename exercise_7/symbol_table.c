#include "symbol_table_types.h"
#include "ast_types.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

node* currentSymbolTable=NULL;


// *** SymbolTable ********************************************************************
void enterScope() {
    printf("enter\n");
    if (currentSymbolTable == NULL){
        printf("if\n");
        node result = create_node(n_ROOT);
        printf("create\n");
        result.outer_scope = NULL;
        printf("null\n");
        //currentSymbolTable = &result;
        if(currentSymbolTable==NULL) currentSymbolTable = malloc(sizeof(node));
        memcpy(currentSymbolTable, &result, sizeof(node));
        printf("adr\n");
    }else{
        printf("else\n");
        node result = create_node(n_CHILD);
        printf("create\n");
        result.outer_scope = currentSymbolTable;
        result.sym_tab = NULL;
        memcpy(currentSymbolTable, &result, sizeof(node));
        printf("adr\n");
    }
}

void leaveScope() {
    printf("leave\n");
    if (PRINTSYMTABLE) printSymbolTable(currentSymbolTable);
    printf("leave 2\n");
    currentSymbolTable = currentSymbolTable->outer_scope;
}

entry* lookupSymbol(char* name) {
    printf("look\n");
    entry* result = NULL;
    node* table = currentSymbolTable;
    while (result == NULL && table != NULL) {
        entry* next = table->sym_tab;
        while (next != NULL) {
            if (next->identifier != NULL && strcmp(next->identifier, name) == 0)
                return next;
            
            next = next->next;
        }
        table = table->outer_scope;
    }
    return result;
}

void checkSymbol(node n) {
    printf("check\n");
    char* name = n.identifier;
    if (n.type == n_IDENTIFIER2)
        name = n.body[0].identifier;

    printf("%s\n",name);
    
    entry* symbol = lookupSymbol(name);
    if (symbol == NULL) {
        fprintf(stderr, "ERROR: could not find variable '%s' in any scope, (line %d)\n", name, line_num);
        exit(1);
    }
}

entry* addSymbolTableEntry(node* symbol, entry_type etype, data_type dtype, const node* value) {
    printf("add\n");
    entry* newEntry = malloc(sizeof(entry));
    newEntry->etype = etype;
    newEntry->dtype = dtype;
    if (symbol->type == n_IDENTIFIER) {
        newEntry->identifier = symbol->identifier;
    } else if (symbol->type == n_IDENTIFIER2) {
        newEntry->etype = n_IDENTIFIER2;
        newEntry->identifier = symbol->body[0].identifier;
    } else {
        printf("UNIMPLEMENTED symbol identifier: %d\n", symbol->type);
        exit(1);
    }
    
    if (value != NULL) {
        if (etype == _CONST && dtype == _INT)
            newEntry->intVal = value->symbol->intVal;
        else if (etype == _CONST && dtype == _BOOL)
            newEntry->intVal = value->symbol->intVal;
        else if (etype == _CONST && dtype == _REAL)
            newEntry->realVal = value->symbol->realVal;
        else if (etype == _SCALAR)
            newEntry->scalar = symbol->identifier;
        else {
            printf("UNIMPLEMENTED symbol combination: %d, %d; %s\n", etype, dtype, symbol->identifier);
            exit(1);
        }
    }
    
    newEntry->next = NULL;
    
    entry* head = currentSymbolTable->sym_tab;
    if (head == NULL) {
        currentSymbolTable->sym_tab = newEntry;
        return newEntry;
    } else {
        
    while (head->next != NULL) {
        head = head->next;
    }
    
    head->next = newEntry;
    return newEntry;
    }
}


char* printEntry(entry* symbol) {
    printf("print\n");
    char tmp[1024] = "";
    strcat(tmp, symbol->identifier);
    strcat(tmp, " ");
    
    if (symbol->etype == _CONST)
       strcat(tmp, "(const: ");
    else if (symbol->etype == _SCALAR)
        strcat(tmp, "(var: ");
    else if (symbol->etype == n_IDENTIFIER2)
        strcat(tmp, "(array: ");
    else
        strcat(tmp, "(UNKNOWN: ");

    if (symbol->dtype == _BOOL)
        strcat(tmp, "bool)");
    else if (symbol->dtype == _INT)
        strcat(tmp, "int)");
    else if (symbol->dtype == _REAL)
        strcat(tmp, "real)");
    else
        strcat(tmp, "UNKNOWN)");
    
    if (symbol->etype == _CONST) {
        if (symbol->dtype == _BOOL || symbol->dtype == _INT) {
            strcat(tmp, "=");
            char tmpVal[10];
            sprintf(tmpVal, "%d", symbol->intVal);
            strcat(tmp, tmpVal);
        } else if (symbol->dtype == _REAL) {
            strcat(tmp, "=");
            char tmpVal[16];
            sprintf(tmpVal, "%f", symbol->realVal);
            strcat(tmp, tmpVal);
        } else
            strcat(tmp, "=UNKNOWN");
    }

    char* result = malloc((strlen(tmp)+1)*sizeof(char));
    strcpy(result, tmp);
    return result;
}

void printSymbolTable(node* innerMost) {
    printf("Symbol Table:\n");
    entry* innerMostEntries = innerMost->sym_tab;
    int countScopes = 0;
    node* scope = innerMost;
    while (scope->outer_scope != NULL) {
        scope = scope->outer_scope;
        countScopes++;
    }
    
    int NBR_COLUMNS = 3;
    int NBR_ROWS = 7;
    char* table[NBR_COLUMNS][NBR_ROWS];
    for (int col = NBR_COLUMNS-1; col >= 0; col--) {
        entry* innerMostEntries = innerMost->sym_tab;
        for (int row = 0; row < NBR_ROWS; row++) {
            if (col == (countScopes-1) && innerMostEntries != NULL) {
                table[col][row] = printEntry(innerMostEntries);
                innerMostEntries = innerMostEntries->next;
            } else {
                table[col][row] = "---";
            }
        }
        if (col == (countScopes-1)) {
            countScopes--;
            innerMost = innerMost->outer_scope;
        }
    }
    
    for (int row = 0; row < NBR_ROWS; row++) {
        for (int col = 0; col < NBR_COLUMNS; col++) {
            printf("| %20s ", table[col][row]);
        }
        printf("\n");
    }

}