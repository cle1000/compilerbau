#ifndef SYMBOL_H
#define SYMBOL_H
#include "ast_types.h"

#define PRINTSYMTABLE true

extern int line_num;

typedef enum {
    _CONST=0,
    _SCALAR,
    _ARRAY
} entry_type;

typedef enum { 
    _BOOL=0, 
    _INT, 
    _REAL 
} data_type;

typedef struct _entry {
    entry_type etype;
    data_type dtype;
    char* identifier;
    union {
        int intVal;
        float realVal;
        char *scalar;
        void *array[2]; /* identifier, index */
    };
    struct _entry *next; /* collision list */
} entry;

entry* addSymbolTableEntry(node*, entry_type, data_type, const node*);
void printSymbolTable(node*);


void checkSymbol(node); //bison
void enterScope(); //bison
void leaveScope(); //bison

#endif //SYMBOL_H
