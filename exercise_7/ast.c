#ifndef AST_C
#define AST_C

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include "ast_types.h"
#include "symbol_table_types.h"

char *names[27] = {
    "n_COMP_STMT",
    "n_ASSIGN",
    "n_ASSIGN_STMT",
    "n_IF",
    "n_FOR",
    "n_WHILE",
    "n_STATEMENT",
    "n_STATEMENT_LIST",
    "n_CONST",
    "n_VAR",
    "n_VAR_LIST",
    "n_DECL",
    "n_DECL_LIST",
    "n_TYPE",
    "n_EXPR",
    "n_INT_CONST",
    "n_REAL_CONST",
    "n_BOOL_CONST",
    "n_STRING_CONST",
    "n_IDENTIFIER",
    "n_IDENTIFIER2",
    "n_OP",
    "n_IOFUNC",
    "n_IOFUNCCALL",
    "n_PARAM_LIST",
    "n_FINAL",
    "n_NONE",
};

node create_str_node(node_type typ, char* str){
  node ast_node;
  ast_node.type = typ;
  ast_node.identifier = str;
  return ast_node;
}

node* create_symbol_node(node_type type) {
    node* result = malloc(sizeof(node));
    result->type = type;
    return result;
}

node create_int_node(node_type typ, int i){
  node ast_node = create_node(typ);
  //ast_node.type = typ;
  //ast_node.iValue = i; //todo
  entry* symbol = malloc(sizeof(entry));
  symbol->intVal = i;
  ast_node.symbol = symbol;
  return ast_node;
}

node create_operator_node(node_type typ, int i){
  node ast_node;
  ast_node.type = typ;
  ast_node.op = i;
  return ast_node;
}

node create_node(node_type typ){
  node ast_node;

  ast_node.type = typ;

  ast_node.body = (node*)malloc(sizeof(node));
  ast_node.body[0].type = n_NONE;

  return ast_node;
}

node create_node1(node_type typ, node body1){
  node ast_node;

  ast_node.type = typ;
  
  ast_node.body = (node*)malloc(2*sizeof(node));
  ast_node.body[0] = body1;
  ast_node.body[1].type = n_NONE;

  return ast_node;
}

node create_node2(node_type typ, node body1, node body2){
  node ast_node;
  ast_node.type = typ;
  ast_node.body = (node*)malloc(3*sizeof(node));
  ast_node.body[0] = body1;
  ast_node.body[1] = body2;
  ast_node.body[2].type = n_NONE;
  return ast_node;
}

node create_node3(node_type typ, node body1, node body2, node body3){
  node ast_node;

  ast_node.type = typ;
  
  ast_node.body = (node*)malloc(4*sizeof(node));
  ast_node.body[0] = body1;
  ast_node.body[1] = body2;
  ast_node.body[2] = body3;
  ast_node.body[3].type = n_NONE;

  return ast_node;
}

node create_node4(node_type typ, node body1, node body2, node body3, node body4){
  node ast_node;

  ast_node.type = typ;
  
  ast_node.body = (node*)malloc(5*sizeof(node));
  ast_node.body[0] = body1;
  ast_node.body[1] = body2;
  ast_node.body[2] = body3;
  ast_node.body[3] = body4;
  ast_node.body[4].type = n_NONE;

  return ast_node;
}

node create_op(int i){
  return create_operator_node(n_OP,i);
}

void print_ast(node n, int intendation){
  bool flag = false;
  node * ptr = NULL;
  //printf("\n%s\n",names[n.type-1]);
  switch(n.type){
    case n_COMP_STMT:
      printf("{\n");
      print_ast(n.body[0],intendation);
      print_ast(n.body[1],intendation);
      printf("\n}");
      break;
    case n_ASSIGN:
      print_ast(n.body[0],intendation);
      printf("=");
      print_ast(n.body[1],intendation);
      break;
    case n_ASSIGN_STMT:
        print_ast(n.body[0],intendation);
        if(n.body[1].type!=n_NONE)
          print_ast(n.body[1],intendation);

      break;
    case n_IF:
      printf("if(");
      print_ast(n.body[0],intendation);
      printf(")\n");
      print_ast(n.body[1],intendation);
      printf("\n");
      if(n.body[2].type!=n_NONE){
        printf("else\n");
        print_ast(n.body[2],intendation);
        //printf("\n");
      }
      break;
    case n_FOR:
      printf("for(");
      print_ast(n.body[0],intendation);
      printf("; ");
      print_ast(n.body[1],intendation);
      printf("; ");
      print_ast(n.body[2],intendation);
      printf(")");
      print_ast(n.body[3],intendation);
      break;
    case n_WHILE:
      printf("while(");
      print_ast(n.body[0],intendation);
      printf(") ");
      print_ast(n.body[1],intendation);
      break;
    case n_STATEMENT:
      break;
    case n_STATEMENT_LIST:
      ptr = n.body;
      while(ptr->type!=n_NONE){
        print_ast(*ptr,intendation);
        flag = ptr->type!=n_COMP_STMT && ptr->type!=n_NONE;
        ptr++;
      }
      if(ptr!=n.body && flag) printf(";\n");
      break;
    case n_CONST:
      break;
    case n_DECL:
      print_ast(n.body[0],intendation);
      print_ast(n.body[1],intendation);
      print_ast(n.body[2],intendation);
      break;
    case n_DECL_LIST:
      ptr = n.body;
      while(ptr->type!=n_NONE){
        print_ast(*ptr,intendation);
        ptr++;
      }
       if(ptr!=n.body) printf(";\n");
      break;
    case n_VAR:

      break;
    case n_VAR_LIST:
      print_ast(n.body[0],intendation);
      if(n.body[1].type!=n_NONE) printf(", ");
      print_ast(n.body[1],intendation);
      if(n.body[2].type!=n_NONE){
         printf("=");
         print_ast(n.body[2],intendation);
      }
      
      break;
    case n_TYPE:
      printf("%s ",n.identifier);
      break;
    case n_EXPR:
      printf("(");
      print_ast(n.body[0],intendation);
      print_ast(n.body[1],intendation);
      if(n.body[2].type!=n_NONE){
        print_ast(n.body[2],intendation);
      }
      printf(")");
      break;
    case n_INT_CONST:
      printf("%i", n.symbol->intVal); //todo
      break;
    case n_REAL_CONST:
      printf("%f", n.symbol->realVal); //todo
      break;
    case n_BOOL_CONST:
      printf((n.symbol->intVal)? "true" : "false"); //todo
      break;
    case n_STRING_CONST:
      printf("%s",n.identifier);
      break;
    case n_IDENTIFIER:
      printf("%s",n.identifier);
      break;
    case n_IDENTIFIER2:
      printf("%s",n.body[0].identifier);
      printf("[");
      print_ast(n.body[1],intendation);
      printf("]");
      break;
    case n_OP:
      switch(n.symbol->intVal){ //todo
        case PLUS:
          printf(" + ");
          break;
        case MINUS:
          printf(" - ");
          break;
        case MUL:
          printf(" * ");
          break;
        case DIV:
          printf(" / ");
          break;
        case MOD:
          printf(" +%"); 
          break;
        case LT:
          printf(" < ");
          break;
        case LE:
          printf(" <= ");
          break;
        case GT:
          printf(" > ");
          break;
        case GE:
          printf(" >= ");
          break;
        case EQ:
          printf(" == ");
          break;
        case NE:
          printf(" != ");
          break;
        case AND:
          printf(" && ");
          break;
        case OR:
          printf(" || ");
          break;
        case PLUSPLUS:
          printf("++");
          break;
        case MINUSMINUS:
          printf("--");
          break;
        case NOT:
          printf("!");
          break;
      }
      break;
    case n_IOFUNC:
      printf((n.symbol->intVal) ? "write": "read");
      break;
    case n_IOFUNCCALL:
      print_ast(n.body[0],intendation);
      printf("(");
      print_ast(n.body[1],intendation);
      printf(")");
      break;
    case n_PARAM_LIST:
      ptr = n.body;
      while(ptr->type!=n_NONE){
        print_ast(*ptr,intendation);
        
        ptr++;
        if(ptr->type!=n_NONE) printf(", ");
      }   
      break;
    case n_FINAL:
      printf("final ");
      break;
    case n_NONE:
      break;
  }

}

//  node * set_next(node* n,node* val, bool body){if(n==NULL){ return NULL; } if(body) n->body=val; else n->next=val; }
//  node * get_next(node * n, bool body){if(n==NULL){ return NULL; } if(body) return n->body; else return n->next;}

// node *last(node *n, bool body){
//   while(get_next(n,body)!=NULL){
//      n=get_next(n,body);   
//   }
   
//   return n;
// }

// node * chain_nodes(node *n1, node *n2, bool body){
//   printf("start chain_call %i\n", n1->type);
//   if(n1==NULL) printf("n1 null\n");
//   if(n2==NULL || n2 == 0) printf("n2 null\n");
//   else printf("n2 %i\n", n1->type);
//   if(n1!=NULL){
//       node *l =last(n1,body);
//       printf("last node %i\n", l->type);
//       set_next(l,n2,body);
//   }
//   return n1;
// }

// node *chain_all_nodes(bool body, node *n1, ...){
//   va_list vl;
//   printf("start chain\n");

//   if(n1==NULL) return NULL;
//   va_start(vl,n1);
//   node *n2 = NULL;
//   node *n3 = n1;
//   do {
//     printf("chain node %i\n", n3->type);
//     n2=(node *) va_arg(vl,node *);
//     n3=chain_nodes(n3,n2, body);
//   } while (n2!=NULL);
//   printf("stop chain\n");
//   return n1;
// }

// node *ast_node (node_type typ, node * b, bool body) {
//   node * ast_node = malloc (sizeof (struct _node));

//   ast_node->type = typ;
//   ast_node->next = NULL;
//   ast_node->body = NULL;
//   set_next(ast_node,b,body);

//   printf("stop node %i\n", typ);
//   return ast_node;
// }

// node *int_ast_node (int val) {
//   node * ast_node = malloc (sizeof (struct _node));

//   ast_node->type = n_INT_CONST;
//   ast_node->iValue = val;
//   ast_node->next = NULL;
//   ast_node->body = NULL;

//   return ast_node;
// }

// node *bool_ast_node (int val) {
//   node * ast_node = malloc (sizeof (struct _node));

//   ast_node->type = n_BOOL_CONST;
//   ast_node->iValue = val;
//   ast_node->next = NULL;
//   ast_node->body = NULL;

//   return ast_node;
// }

// node *OP_ast_node (int val) {
//   node * ast_node = malloc (sizeof (struct _node));

//   ast_node->type = n_OP;
//   ast_node->iValue = val;
//   ast_node->next = NULL;
//   ast_node->body = NULL;

//   return ast_node;
// }

// node *float_ast_node (float val) {
//   node * ast_node = malloc (sizeof (struct _node));

//   ast_node->type = n_REAL_CONST;
//   ast_node->fValue = val;
//   ast_node->next = NULL;
//   ast_node->body = NULL;

//   return ast_node;
// }

// node *str_ast_node (char * ident, bool identifier) {
//   node * ast_node = malloc (sizeof (struct _node));

//   if(identifier)
//   	ast_node->type = n_IDENTIFIER;
//   else
//   	ast_node->type = n_STRING_CONST;

//   ast_node->identifier=ident;
//   ast_node->next = NULL;
//   ast_node->body = NULL;

//   return ast_node;
// }

// node *typ_ast_node (char * typ) {
//   node * ast_node = malloc (sizeof (struct _node));

//   ast_node->type = n_TYPE;
//   ast_node->identifier=typ;
//   ast_node->next = NULL;
//   ast_node->body = NULL;

//   return ast_node;
// }


#endif //AST_C