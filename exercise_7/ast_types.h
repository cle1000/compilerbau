#ifndef AST_TYPES_H
#define AST_TYPES_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>

typedef enum {
    n_COMP_STMT = 1,
    n_ASSIGN,
    n_ASSIGN_STMT,
    n_IF,
    n_FOR,
    n_WHILE,
    n_STATEMENT,
    n_STATEMENT_LIST,
    n_CONST,
    n_VAR,
    n_VAR_LIST,
    n_DECL,
    n_DECL_LIST,
    n_TYPE,
    n_EXPR,
    n_INT_CONST,
    n_REAL_CONST,
    n_BOOL_CONST,
    n_STRING_CONST,
    n_IDENTIFIER,
    n_IDENTIFIER2,
    n_OP,
    n_IOFUNC,
    n_IOFUNCCALL,
    n_PARAM_LIST,
    n_FINAL,
    n_NONE,
    n_ROOT,
    n_CHILD,
} node_type;

typedef enum {
    PLUS = 0,
    MINUS,
    MUL,
    DIV,
    MOD,
    LT,
    LE,
    GT,
    GE,
    EQ,
    NE,
    AND,
    OR,
    PLUSPLUS,
    MINUSMINUS,
    NOT,
} operator;

typedef struct _node {
    node_type type;
    struct _entry *sym_tab;            /* symbol table for COMP_STMT */
    struct _node *outer_scope;           /* pointer to the outer block */
    struct _entry *symbol;
    union{
        operator op;
        char* identifier; 
        /* list of BNF right-hand side symbols of nonterminal type */
        struct _node *body;
    };
    struct _node* next; /* decl-list, stmt-list */
} node;

node create_str_node(node_type typ, char *);
node create_int_node(node_type typ, int);
node create_node(node_type typ);
node create_node1(node_type typ, node body1);
node create_node2(node_type typ, node body1, node body2);
node create_node3(node_type typ, node body1, node body2, node body3);
node create_node4(node_type typ, node body1, node body2, node body3, node body4);
node create_op(int);

void print_ast(node,int);

#endif //AST_TYPES_H