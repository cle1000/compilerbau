%{
#include "y.tab.h"

extern int yyerror(const char *);
extern int fileno(FILE *stream);

%}

whitespace [\t\n\r ]
number [0-9]+
singles [-+()*]

%%

{whitespace} ;

{singles} { return *yytext; }
{number}  { yylval.intVal=atoi(yytext); return NUMBER; }

. {
  yyerror("Invalid symbol");
}

%%
