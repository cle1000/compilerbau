%{

#include <stdio.h>
#include <stdlib.h>

/* Define to prevent warnings */
extern int yyparse(void);
extern int yylex(void);
extern char* yytext;

void yyerror(const char *str){
  fprintf(stderr,"ERROR: %s: \"%s\"\n", str, yytext);
}

int yywrap(){
  return 1;
}

void fail(char *msg) {
  fprintf(stderr, "%s\n", msg);
  exit(1);
}

typedef enum { PLUS, MINUS, TIMES, NEGATION } operator;
typedef struct _stack { operator op; struct _stack* nxt; } stack;

void push(stack** st, operator op){
  stack* new = malloc(sizeof(stack));
  new->op = op;
  new->nxt = *st;
  *st = new;
}

operator peek(stack** st){
  if (!st || !*st) fail("operator peek failed");
  return (*st)->op;
}

operator pop(stack** st){
  if (!st || !*st) fail("operator pop failed");
  operator r = (*st)->op;
  *st = (*st)->nxt;
  return r;
}

int expressionValue;
stack* operators;

int main(){
  yyparse();
  printf("%d\n", expressionValue);
  return 0;
}

%}

%union {
  int intVal;
};

%token <intVal> NUMBER

%type <intVal> expression operand operandList
%type <void> start operator


%start start

%%

start: expression { expressionValue = $1; }

expression: '(' operator operandList ')' { if (pop(&operators) == NEGATION)
                                             $$ = - $3;
                                           else
                                             $$ = $3; }

operandList: operand              { $$ = $1; }
           | operand operandList  { switch (peek(&operators)) {
                                      case PLUS:
                                      case MINUS:
                                        $$ = $1 + $2;
                                        break;
                                      case NEGATION:
                                        /* Correct wrong assumption */
                                        pop(&operators);
                                        push(&operators, MINUS);
                                        $$ = $1 - $2;
                                        break;
                                      case TIMES:
                                        $$ = $1 * $2;
                                        break;
                                      default:
                                        fail("switch case insufficient");
                                        break;
                                    }
                                  }

operand: NUMBER     { $$ = $1; }
       | expression { $$ = $1; }

operator: '+' { push(&operators, PLUS); }
        | '-' { push(&operators, NEGATION); } /* Assume negation */
        | '*' { push(&operators, TIMES); }

%%

