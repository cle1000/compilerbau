
%option yylineno
%{
#include <stdio.h>
#include <string.h>

	int opencall = 0;
	char errors [99999];

char* upper_string(char s[]) {
	int c = 0;
	while (s[c] != '\0') {
		if (s[c] >= 'a' && s[c] <= 'z') {
			s[c] = s[c] - 32;
		}
		c++;
	}
	return s;
}

char* add_error_message(int linenumber, char * line){
	char buf [30];
	sprintf(buf, "Error at line: %d - %s\n", linenumber, line);
	strcat(errors,buf);
}

char* lower_string(char s[]) {
	int c = 0;
	while (s[c] != '\0') {
		if (s[c] >= 'A' && s[c] <= 'Z') {
			s[c] = s[c] + 32;
		}
		c++;
	}
	return s;
}

%}

tabs				[\t]+
newlines			[\n]+
spaces				[ ]+

bool_value          (true)|(false)
string              \".*\"

DATA_TYPE           int|boolean|float

IDENTIFIER			([A-Za-z_])([A-Za-z0-9_]*)
NUMBER				([0-9])([0-9]*)
OPERATOR			\+ | ==

semicolon			;
comma               ,

allocation          =|:=
for					(for)\(
ifcall				(if)\(
elsecall			(else)
methodcall          {IDENTIFIER}\(
closecall           (\))
c_equal				(==)
c_unequal			(!=)
c_leq				(<=)
c_geq				(>=)
c_ge				(>)
c_le				(<)
comparison			{c_equal}|{c_unequal}|{c_leq}|{c_geq}|{c_ge}|{c_le}
a_plusplus			\+\+
a_plus				\+
a_minusminus		\-\-
a_minus				-
a_div				\/
a_mul				\*
a_percent           \%
arithmetic          {a_plus}|{a_plusplus}|{a_minus}|{a_minusminus}|{a_div}|{a_mul}|{a_percent}
and					\&\&
or					\|\|
boolean_operator    {and}|{or}
oarray              \[
carray              \]
f_final				(final)
flags               {f_final}
any					.*




%%
"/*".*"*/"				printf("/** %s ", (yytext+2));
"//".*					printf("/** %s */", (yytext+2));
{flags}					printf("%s", upper_string(yytext));
{string}				ECHO;
{bool_value}			ECHO;
{NUMBER}				ECHO;
{methodcall}            opencall++; printf("%s", upper_string(yytext));
{elsecall}				printf("%s", upper_string(yytext));
{closecall}				if (opencall == 0){ add_error_message(yylineno, yytext);} opencall--; ECHO;
{allocation}			ECHO;
{comparison}			ECHO;
{arithmetic}			ECHO;
{boolean_operator}		ECHO;
{oarray}				ECHO;
{DATA_TYPE}				printf("%s", upper_string(yytext));
{carray}				ECHO;
\{                      ECHO;
\}                      ECHO;
{semicolon}             ECHO;
{comma}					ECHO;
{newlines}              printf("\n");
^{tabs}					ECHO;
{tabs}					printf("\t");
^{spaces}				ECHO;
{spaces}				printf(" ");
{IDENTIFIER}			printf("%s", lower_string(yytext));
.						add_error_message(yylineno, yytext);
%%

int main(int argc, char **argv) {
	int r = yylex();
	printf("\n%s", errors);
	return r;
}